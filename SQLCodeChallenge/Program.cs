﻿using SQLInMemorySupport.Engines;
using System;
using System.Threading.Tasks;

namespace SQLCodeChallenge
{
    public class SQLChallenge
    {
        public string SQLScriptToRun()
        {
            throw new NotImplementedException("Add your script here");
        }

        static async Task Main(string[] args)
        {
            SQLChallenge challenge = new SQLChallenge();

            var script = challenge.SQLScriptToRun();

            SQLEngine engine = new SQLEngine();

            try
            {
                var rowData = await engine.RunSQLScriptAsync(script);
                var headers = engine.Headers;

                Console.WriteLine(headers);
                Console.WriteLine(rowData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }
    }
}

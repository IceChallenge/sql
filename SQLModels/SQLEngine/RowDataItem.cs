﻿namespace SQLModels.SQLEngine
{
    public class RowDataItem
    {
        public string Director { get; set; }
        public int NumberOfFilms { get; set; }
    }
}

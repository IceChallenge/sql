﻿namespace SQLModels
{
    public partial class Movies
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DirectorId { get; set; }

        public virtual Directors Director { get; set; }
    }
}

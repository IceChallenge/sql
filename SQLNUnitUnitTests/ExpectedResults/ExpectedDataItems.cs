﻿using SQLModels.SQLEngine;
using System.Collections.Generic;

namespace SQLNUnitUnitTests.ExpectedResults
{
    public class ExpectedDataItems
    {
        public List<RowDataItem> ExampleCase { get; set; }
        public List<RowDataItem> EveryDirectorHasDirected { get; set; }
        public List<RowDataItem> SomeDirectorsHaveYetToDirect { get; set; }
        public List<RowDataItem> SomeDirectorsHaveSameName { get; set; }


        public void PopulateExampleCase()
        {
            this.ExampleCase = new List<RowDataItem>();

            RowDataItem data1 = new RowDataItem() { Director = "Michael Curtiz", NumberOfFilms = 2 };
            RowDataItem data2 = new RowDataItem() { Director = "Alfred Hitchcock", NumberOfFilms = 1 };
            RowDataItem data3 = new RowDataItem() { Director = "Steven Spielberg", NumberOfFilms = 0 };

            ExampleCase.AddRange(new List<RowDataItem>() { data1, data2, data3 });
        }

        public void PopulateTestCase2()
        {
            this.EveryDirectorHasDirected = new List<RowDataItem>();

            RowDataItem data1 = new RowDataItem() { Director = "Director1", NumberOfFilms = 1 };
            RowDataItem data2 = new RowDataItem() { Director = "Director2", NumberOfFilms = 1 };
            RowDataItem data3 = new RowDataItem() { Director = "Director3", NumberOfFilms = 1 };
            RowDataItem data4 = new RowDataItem() { Director = "Director4", NumberOfFilms = 1 };

            EveryDirectorHasDirected.AddRange(new List<RowDataItem>() { data1, data2, data3, data4 });
        }

        public void PopulateTestCase3()
        {
            this.SomeDirectorsHaveYetToDirect = new List<RowDataItem>();

            RowDataItem data1 = new RowDataItem() { Director = "Director1", NumberOfFilms = 1 };
            RowDataItem data2 = new RowDataItem() { Director = "Director2", NumberOfFilms = 1 };
            RowDataItem data3 = new RowDataItem() { Director = "Director3", NumberOfFilms = 1 };
            RowDataItem data4 = new RowDataItem() { Director = "Director4", NumberOfFilms = 1 };
            RowDataItem data5 = new RowDataItem() { Director = "Director5", NumberOfFilms = 0 };

            SomeDirectorsHaveYetToDirect.AddRange(new List<RowDataItem>() { data1, data2, data3, data4, data5 });
        }

        public void PopulateTestCase4()
        {
            this.SomeDirectorsHaveSameName = new List<RowDataItem>();

            RowDataItem data1 = new RowDataItem() { Director = "Director1", NumberOfFilms = 1 };
            RowDataItem data2 = new RowDataItem() { Director = "Director2", NumberOfFilms = 1 };
            RowDataItem data3 = new RowDataItem() { Director = "Director3", NumberOfFilms = 1 };
            RowDataItem data4 = new RowDataItem() { Director = "Director4", NumberOfFilms = 1 };
            RowDataItem data5 = new RowDataItem() { Director = "Director4", NumberOfFilms = 1 };

            SomeDirectorsHaveSameName.AddRange(new List<RowDataItem>() { data1, data2, data3, data4, data5 });

        }


    }
}

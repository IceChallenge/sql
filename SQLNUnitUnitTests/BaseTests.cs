﻿using Microsoft.EntityFrameworkCore.Internal;
using NUnit.Framework;
using SQLInMemorySupport.DB;
using SQLInMemorySupport.Engines;
using SQLModels.SQLEngine;
using SQLNUnitUnitTests.ExpectedResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQLNUnitUnitTests
{
    internal abstract class BaseTests
    {
        #region Activator Support
        public string FullyQualifiedClassName { get; set; }

        private object NewClassInstance(string fullyQualifiedName)
        {

            Type type = Type.GetType(fullyQualifiedName);

            if (type != null)
            {
                return Activator.CreateInstance(type);
            }

            return null;
        }
        #endregion

        [Test]
        public async Task ExampleCaseAsync()
        {
            ExpectedDataItems expectedData = new ExpectedDataItems();
            expectedData.PopulateExampleCase();

            dynamic main = NewClassInstance(FullyQualifiedClassName);

            var script = main.SQLScriptToRun();

            SQLEngine engine = new SQLEngine();
            
            try
            {
                var dataResults = await engine.RunSQLScriptAsync(script);

                foreach (var item in engine.RowColumnData)
                {
                    RowDataItem data = (RowDataItem)item.Key;

                    //string message = string.Format("Director: {0} / Number of Films: {1}", data.Director, data.NumberOfFilms);
                    //Console.WriteLine(message);
                    //System.Diagnostics.Debug.WriteLine(message);

                    var lookup = expectedData.ExampleCase.FirstOrDefault(r => r.Director == data.Director && r.NumberOfFilms == data.NumberOfFilms);
                    if (lookup != null)
                    {
                        expectedData.ExampleCase.Remove(lookup);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (expectedData.ExampleCase.Count == 0)
            {
                Assert.Pass();
            }
            
            Assert.Fail();
        }

        [Test]
        public async Task EveryDirectorHasDirectedAsync()
        {
            int testCase = 2;
            ExpectedDataItems expectedData = new ExpectedDataItems();
            expectedData.PopulateTestCase2();

            dynamic main = NewClassInstance(FullyQualifiedClassName);

            var script = main.SQLScriptToRun();

            SQLEngine engine = new SQLEngine();

            try
            {
                var dataResults = await engine.RunSQLScriptAsync(script, testCase);

                Console.WriteLine("Data Result rows: " + engine.RowColumnData.Count);

                foreach (var item in engine.RowColumnData)
                {
                    RowDataItem data = (RowDataItem)item.Key;

                    string message = string.Format("Director: {0} / Number of Films: {1}", data.Director, data.NumberOfFilms);
                    Console.WriteLine(message);
                    System.Diagnostics.Debug.WriteLine(message);

                    var lookup = expectedData.EveryDirectorHasDirected.FirstOrDefault(r => r.Director == data.Director && r.NumberOfFilms == data.NumberOfFilms);
                    if (lookup != null)
                    {
                        expectedData.EveryDirectorHasDirected.Remove(lookup);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (expectedData.EveryDirectorHasDirected.Count == 0)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public async Task SomeDirectorsHaveYetToDirectAsync()
        {
            int testCase = 3;
            ExpectedDataItems expectedData = new ExpectedDataItems();
            expectedData.PopulateTestCase3();

            dynamic main = NewClassInstance(FullyQualifiedClassName);

            var script = main.SQLScriptToRun();

            SQLEngine engine = new SQLEngine();

            try
            {
                var dataResults = await engine.RunSQLScriptAsync(script, testCase);

                Console.WriteLine("Data Result rows: " + engine.RowColumnData.Count);

                foreach (var item in engine.RowColumnData)
                {
                    RowDataItem data = (RowDataItem)item.Key;

                    string message = string.Format("Director: {0} / Number of Films: {1}", data.Director, data.NumberOfFilms);
                    Console.WriteLine(message);
                    System.Diagnostics.Debug.WriteLine(message);

                    var lookup = expectedData.SomeDirectorsHaveYetToDirect.FirstOrDefault(r => r.Director == data.Director && r.NumberOfFilms == data.NumberOfFilms);
                    if (lookup != null)
                    {
                        expectedData.SomeDirectorsHaveYetToDirect.Remove(lookup);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (expectedData.SomeDirectorsHaveYetToDirect.Count == 0)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public async Task SomeDirectorsHaveSameNameAsync()
        {
            int testCase = 4;
            ExpectedDataItems expectedData = new ExpectedDataItems();
            expectedData.PopulateTestCase4();

            dynamic main = NewClassInstance(FullyQualifiedClassName);

            var script = main.SQLScriptToRun();

            SQLEngine engine = new SQLEngine();

            try
            {
                var dataResults = await engine.RunSQLScriptAsync(script, testCase);

                Console.WriteLine("Data Result rows: " + engine.RowColumnData.Count);

                foreach (var item in engine.RowColumnData)
                {
                    RowDataItem data = (RowDataItem)item.Key;

                    string message = string.Format("Director: {0} / Number of Films: {1}", data.Director, data.NumberOfFilms);
                    Console.WriteLine(message);
                    System.Diagnostics.Debug.WriteLine(message);

                    var lookup = expectedData.SomeDirectorsHaveSameName.FirstOrDefault(r => r.Director == data.Director && r.NumberOfFilms == data.NumberOfFilms);
                    if (lookup != null)
                    {
                        expectedData.SomeDirectorsHaveSameName.Remove(lookup);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (expectedData.SomeDirectorsHaveSameName.Count == 0)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

    }
}

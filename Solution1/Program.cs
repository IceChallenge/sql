﻿using SQLInMemorySupport.Engines;
using System;
using System.Threading.Tasks;

namespace Solution1
{
    public class SQLChallenge
    {
        public string SQLScriptToRun()
        {
            string script = "";

            script += "SELECT directorData.name as Director, count(m.directorId) as 'number of films' ";
            script += "FROM ";
            script += "(SELECT d.id, d.Name FROM Directors d) directorData ";
            script += "LEFT JOIN Movies m ";
            script += "ON m.directorId = directorData.id ";
            script += "GROUP BY directorData.name, directorData.id";
            
            return script;
        }

        static async Task Main(string[] args)
        {
            SQLChallenge challenge = new SQLChallenge();

            var script = challenge.SQLScriptToRun();

            SQLEngine engine = new SQLEngine();

            try
            {
                var rowData = await engine.RunSQLScriptAsync(script);
                var headers = engine.Headers;

                Console.WriteLine(headers);
                Console.WriteLine(rowData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}

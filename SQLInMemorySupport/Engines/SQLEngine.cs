﻿using Microsoft.EntityFrameworkCore;
using SQLInMemorySupport.Contexts;
using SQLInMemorySupport.DB;
using SQLModels.SQLEngine;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SQLInMemorySupport.Engines
{
    public class SQLEngine
    {
        private Dictionary<string, int> ColumnHeaders { get; set; }
        public string Headers
        {
            get
            {
                return ProcessColumnHeaders();
            }
        }
        public Dictionary<object, int> RowColumnData { get; private set; }

        public SQLEngine()
        {
            this.ColumnHeaders = new Dictionary<string, int>();
            this.RowColumnData = new Dictionary<object, int>();
        }

        public async Task<string> RunSQLScriptAsync(string script, int testCase = 1)
        {
            string dataResult = "";
            
            Console.WriteLine("Initializing Database In-Memory, stand by.. " + DateTime.Now);

            SqliteInMemory db = new SqliteInMemory();

            DataItems data = new DataItems();
            await data.SeedAsync(db.Options, testCase);

            using (var context = new ChallengeContext(db.Options))
            {
                Console.WriteLine("Database ready.. " + DateTime.Now);

                var cmd = context.Database.GetDbConnection().CreateCommand();
                cmd.CommandText = script;

                Console.WriteLine("Running SQL Script.. " + Environment.NewLine);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        //var schemaTableReport = reader.GetSchemaTable();
                        //foreach (DataRow row in schemaTableReport.Rows)
                        //{
                        //    foreach (DataColumn column in schemaTableReport.Columns)
                        //    {
                        //        Console.WriteLine(string.Format("{0} = {1}", column.ColumnName, row[column]));
                        //    }
                        //}

                        while (reader.Read())
                        {
                            var fieldCount = reader.FieldCount;
                            var schemaTable = reader.GetSchemaTable();
                            string columnItem = "";
                            //string baseSchemaItem = "";

                            RowDataItem rowData = new RowDataItem();

                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (i > 0)
                                {
                                    // Data Result is CSV based
                                    dataResult += ",";
                                }

                                // Get Column Data value
                                dataResult += reader.GetString(i);

                                // Save Column Header
                                columnItem = schemaTable.Rows[i]["ColumnName"].ToString();
                                if (!ColumnHeaders.ContainsKey(columnItem))
                                {
                                    ColumnHeaders.Add(columnItem, ColumnHeaders.Count + 1);
                                }

                                if (columnItem.Equals("director", StringComparison.OrdinalIgnoreCase))
                                {
                                    rowData.Director = reader.GetString(i);
                                }

                                if (columnItem.Equals("number of films", StringComparison.OrdinalIgnoreCase))
                                {
                                    rowData.NumberOfFilms = reader.GetInt32(i);
                                }

                            }

                            RowColumnData.Add(rowData, RowColumnData.Count + 1);

                            dataResult += Environment.NewLine;
                        }

                    }
                    else
                    {
                        Console.WriteLine("No results");
                    }
                }

            }

            return dataResult;
        }

        private string ProcessColumnHeaders()
        {
            string header = "";

            if (this.ColumnHeaders.Count > 0)
            {
                foreach (var item in this.ColumnHeaders)
                {
                    header += string.IsNullOrEmpty(header) ? "" : ",";
                    header += item.Key;
                }
            }
            return header;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SQLInMemorySupport.Contexts;
using SQLModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SQLInMemorySupport.DB
{
    public class DataItems
    {
        public async Task SeedAsync(DbContextOptions<ChallengeContext> options, int testCase)
        {
            using (var context = new ChallengeContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                List<Directors> directors = null;
                List<Movies> movies = null;

                switch (testCase)
                {
                    case 1: // Example Case
                        directors = TestCase1Directors();
                        movies = TestCase1Movies();
                        break;

                    case 2: // Every director has directed a movie
                        directors = TestCase2Directors();
                        movies = TestCase2Movies();
                        break;

                    case 3: // Some Directors have yet to direct a movie
                        directors = TestCase3Directors();
                        movies = TestCase2Movies();
                        break;

                    case 4: // Some directors have the same name
                        directors = TestCase4Directors();
                        movies = TestCase4Movies();
                        break;

                }

                foreach (Directors item in directors)
                {
                    await context.AddAsync(item);
                }

                foreach (Movies item in movies)
                {
                    await context.AddAsync(item);
                }

                await context.SaveChangesAsync();
            }
        }

        private List<Directors> TestCase1Directors()
        {
            return new List<Directors> 
            {
                new Directors { Id = 1, Name = "Michael Curtiz" },
                new Directors { Id = 2, Name = "Alfred Hitchcock" },
                new Directors { Id = 3, Name = "Steven Spielberg" },
            };
        }

        private List<Movies> TestCase1Movies()
        {
            return new List<Movies>
            {
                new Movies { Id = 1, Name = "Casablanca", DirectorId = 1 },
                new Movies { Id = 2, Name = "Yankee Doodle Dandy", DirectorId = 1 },
                new Movies { Id = 3, Name = "Psycho", DirectorId = 2 }

            };
        }

        private List<Directors> TestCase2Directors()
        {
            // Add one more Director/Movie not discussed in Challenge so logic can't be hard coded
            // for a Test Case that "Every Director has directed a movie"

            return new List<Directors>
            {
                new Directors { Id = 1, Name = "Director1" },
                new Directors { Id = 2, Name = "Director2" },
                new Directors { Id = 3, Name = "Director3" },
                new Directors { Id = 4, Name = "Director4" }
            };
        }

        private List<Movies> TestCase2Movies()
        {
            return new List<Movies>
            {
                new Movies { Id = 1, Name = "Movie1", DirectorId = 1 },
                new Movies { Id = 2, Name = "Movie2", DirectorId = 2 },
                new Movies { Id = 3, Name = "Movie3", DirectorId = 3 },
                new Movies { Id = 4, Name = "Movie4", DirectorId = 4 }

            };
        }

        private List<Directors> TestCase3Directors()
        {
            return new List<Directors>
            {
                new Directors { Id = 1, Name = "Director1" },
                new Directors { Id = 2, Name = "Director2" },
                new Directors { Id = 3, Name = "Director3" },
                new Directors { Id = 4, Name = "Director4" },
                new Directors { Id = 5, Name = "Director5" }
            };
        }

        private List<Directors> TestCase4Directors()
        {
            // Add one more Director/Movie not discussed in Challenge so logic can't be hard coded
            // for a Test Case that "Every Director has directed a movie"

            return new List<Directors>
            {
                new Directors { Id = 1, Name = "Director1" },
                new Directors { Id = 2, Name = "Director2" },
                new Directors { Id = 3, Name = "Director3" },
                new Directors { Id = 4, Name = "Director4" },
                new Directors { Id = 5, Name = "Director4" }
            };
        }

        private List<Movies> TestCase4Movies()
        {
            // Movies
            var movies1 = new Movies() { Id = 1, Name = "Movie1", DirectorId = 1 };
            var movies2 = new Movies() { Id = 2, Name = "Movie2", DirectorId = 2 };
            var movies3 = new Movies() { Id = 3, Name = "Movie3", DirectorId = 3 };
            var movies4 = new Movies() { Id = 4, Name = "Movie4", DirectorId = 4 };
            var movies5 = new Movies() { Id = 5, Name = "Movie5", DirectorId = 5 };

            return new List<Movies>() { movies1, movies2, movies3, movies4, movies5 };
        }



    }
}

﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using SQLInMemorySupport.Contexts;
using System;
using System.Data.Common;

namespace SQLInMemorySupport.DB
{
    public class SqliteInMemory : IDisposable
    {
        private DbContextOptionsBuilder<ChallengeContext> _builder { get; set; }
        private readonly DbConnection _connection;

        public DbContextOptions<ChallengeContext> Options { get; private set; }

        public SqliteInMemory()
        {
            _builder = new DbContextOptionsBuilder<ChallengeContext>();
            _connection = CreateInMemoryDatabase();
            _builder.UseSqlite(_connection);
            this.Options = _builder.Options;
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        public void Dispose() => _connection.Dispose();

    }
}

Visual Studio 2019 .NET Core 3.1 Console Application in C#

# Projects

    SQLCodeChallenge - Code Challenge for Developer

    SQLNUnitUnitTests - Test cases you can run against the Code Challenge
                      - Test cases you can run against Solution1 to see how each solution works

    Solution1 - One solution that solves each Test Case

    SQLModels - Data Models used for In-Memory Database using Sqlite

    SQLInMemorySupport - Support library for In-Memory Database using Sqlite

# Instructions

    Open the "SQLCodeChallenge" Project    
    Set your timer at 30:00 minutes and then start this exercise with the details below
    Run Unit Tests for "TestCasesForChallenge" to test your results

# Code Challenge

    A movie database has the following schema:

        TABLE directors
            id INTEGER NOT NULL PRIMARY KEY,
            name VARCHAR(30) NOT NULL

        TABLE movies
            id INTEGER NOT NULL PRIMARY KEY,
            name VARCHAR(30) NOT NULL,
            directorId INTEGER NOT NULL REFERENCES(directors)

    Write a query that selects each director's name and the total number of films they have directed.  The results should include directors who have not directed films.

    See the xample case for more details

# Example Case

    -- Suggested testing environment:
    -- http://sqlite.online/

    -- Example case create statement:
    CREATE TABLE directors (
        id INTEGER NOT NULL PRIMARY KEY,
        name VARCHAR(30) NOT NULL
    );

    CREATE TABLE movies (
        id INTEGER NOT NULL PRIMARY KEY,
        name VARCHAR(30) NOT NULL,
        directorid INTEGER NOT NULL REFERENCES directors (id)
    );

    INSERT INTO directors(id, name) VALUES (1, 'Michael Curtiz');
    INSERT INTO directors(id, name) VALUES (2, 'Alfred Hitchcock');
    INSERT INTO directors(id, name) VALUES (3, 'Steven Spielberg');

    INSERT INTO movies(id, name, directorid) VALUES(1, 'Casablanca', 1);
    INSERT INTO movies(id, name, directorid) VALUES(2, 'Yankee Doodle Dandy', 1);
    INSERT INTO movies(id, name, directorid) VALUES(3, 'Psycho', 2);

    -- Expected output (in any order):
    -- director         number of films
    -- ------------------------------
    -- Michael Curtiz   2
    -- Alfred Hitchcock 1
    -- Steven Spielberg 0

    -- Explanation:
    -- In this example.
    -- Casablanca and Yankee Doodle Dandy were both directed by Michael Curtiz, so he has two films.
    -- Alfred Hitchcock has directed only Psycho, so he has one film.
    -- Steven Spielberg has not directed any films, so he has a total of 0.

